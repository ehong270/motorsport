import videoBg from './assets/videoplayback.mp4'
import cente from './assets/tirin.jpg'
import lambo from './assets/lambo.png'
import bug from './assets/bug.png'
import ferrari from './assets/ferrari.png'
import React from 'react'
import './index.css'
import Fullpage,{ FullPageSections, FullpageSection, FullpageNavigation } from '@ap.cx/react-fullpage'
import Carousel from 'react-bootstrap/Carousel'
import BlackCar from './assets/campbell-3ZUsNJhi_Ik-unsplash.jpg'
import M from './assets/samuele-errico-piccarini-FMbWFDiVRPs-unsplash.jpg'
import OL from './assets/marcus-p-oUBjd22gF6w-unsplash.jpg'
// import Lambo_QR from './assets/lambo qr.jpg'

function MainPage() {
  const SectionStyle ={
    height: '100vh',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  }

  return (
    <Fullpage>
      <FullpageNavigation/>
      <FullPageSections>

        <FullpageSection style={SectionStyle}>
          <div className='main'>
            {/* <div className='overlay'></div> */}
            <video src={videoBg} autoPlay loop muted/>

              <div className='content'>

                <h1 className='size'>Welcome</h1>
              </div>

              <div className='content2'>
                <h1 className='size'>See our cars</h1>
              </div>

              <div className='content3'>
              <h1 className='size'>NextGen Motors</h1>
              </div>

            </div>
        </FullpageSection>

        <FullpageSection>
          <div className='main'>
            <div className='overlay' />
            <div className='row' >
              <div className='column'>
              <img className='image' src={lambo} />
              </div>
              <div className='column'>
              <img className='image' src={bug} />
              </div>
              <div className='column'>
              <img className='image' src={ferrari} />
              </div>
            </div>



            <img className='image2' src={cente} />
            <div className='word'>
              <h1 className='size' 
              // style={{marginTop:"40px"}}
              >Our Manufacturers</h1>

            </div>

          </div>
        </FullpageSection>
        <FullpageSection style={SectionStyle}>
          <Carousel fade>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src= {BlackCar}
                alt="First slide"
                style={{width:500, height:500, objectFit: 'contain'}}
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src= {M}
                alt="Second slide"
                style={{width:500, height:500, objectFit: 'contain'}}
              />
            </Carousel.Item>
            <Carousel.Item>
              <img
                className="d-block w-100"
                src={OL}
                alt="Third slide"
                style={{width:500, height:500, objectFit: 'scale-down'}}
              />
            </Carousel.Item>
          </Carousel>
        </FullpageSection>

      </FullPageSections>
    </Fullpage>


  );
}

export default MainPage;
